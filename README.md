## Table of Contents

- [Installation](#installation)
- [Features](#features)
  - [Disqus](#disqus)
  - [Google Analytics](#google-analytics)

********************
Shuttersmith Hugo Theme
********************

## Installation

#### With `git`

From the root of your Hugo site, clone the theme into `themes/hugo-shuttersmith` by running :
```
git clone https://gitlab.com/davepwsmith/hugo-shuttersmith.git themes/hugo-shuttersmith
```

#### Manual

1. [Download][zip-archive] zip archive.
2. Unarchive it.
3. Move `hugo-shuttersmith` folder in `themes` folder of your blog

For more information read the official [setup guide][hugo-guide] of Hugo.



## Features

### Disqus

To use this feature, uncomment and fill out the `disqusShortname` parameter in config.toml`.

### Google Analytics

To add Google Analytics, simply sign up to [Google Analytics][g-analytics] to obtain your Google Tracking ID, and add this tracking ID to the `googleAnalytics` parameter in `config.toml`.

