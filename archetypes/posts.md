---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
linktitle: "{{ .TranslationBaseName | safeURL}}"
date: {{ .Date.Format "2017-01-02" }}
draft: true
author: "David Smith"
pageimage: ""
tags: []
highlight: false
---